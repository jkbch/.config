""""""""""""""""""""""""""""""""""""""""""""""""""
"                   Vim config                   "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Set relative number
set number relativenumber

" Set system clipboard as clipboard
set clipboard+=unnamedplus

" Allow hidden buffers
set hidden

" Set wider color range
set termguicolors

" Do not wrap long lines
set nowrap

" Set tab to four spaces
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent

" Custom tab width for filetypes
autocmd FileType json setlocal shiftwidth=2 softtabstop=2

" Ignore case when searching
set ignorecase
set smartcase

" Split below or to the right
set splitbelow
set splitright

" Auto new line after 80 characters
" Select block and use gq to aline block
set tw=80

" Delete trailing whitespace and newlines on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritepre * %s/\n\+\%$//e

" Switch windows with ctrl+h|j|k|l
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Switch buffer with tab/shift+tab
nnoremap <silent> <TAB> :bnext<CR>
nnoremap <silent> <S-TAB> :bprevious<CR>

" Replace all is aliased to S.
nnoremap S :%s//g<Left><Left>

" Allow saving of files as sudo
cmap w!! w !sudo tee > /dev/null %

" Save and close buffer
command Wd write|bdelete

" Toggle dark/light background
function ToggleBackground()
    let &background = ( &background == "dark" ? "light" : "dark" )
endfunction

" Wayland clipboard provider that strips carriage returns (GTK3 issue).
" This is needed because currently there's an issue where GTK3 applications on
" Wayland contain carriage returns at the end of the lines (this is a root
" issue that needs to be fixed).
let g:clipboard = {
      \   'name': 'wayland-strip-carriage',
      \   'copy': {
      \      '+': 'wl-copy --foreground --type text/plain',
      \      '*': 'wl-copy --foreground --type text/plain --primary',
      \    },
      \   'paste': {
      \      '+': {-> systemlist('wl-paste --no-newline | tr -d "\r"')},
      \      '*': {-> systemlist('wl-paste --no-newline --primary | tr -d "\r"')},
      \   },
      \   'cache_enabled': 1,
      \ }

""""""""""""""""""""""""""""""""""""""""""""""""""
"                Leader mappings                 "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Set leader to space
let g:mapleader = "\<Space>"
let g:maplocalleader = "\<Space>"

" Vim mappings
nnoremap <leader>.  :<C-u>e $MYVIMRC<CR>
nnoremap <leader>i  :<C-u>call ToggleBackground()<CR>
nnoremap <leader>s  :<C-u>update<CR>
nnoremap <leader>S  :<C-u>w!!<CR>
nnoremap <leader>q  :<C-u>wq<CR>
nnoremap <leader>Q  :<C-u>q!<CR>
nnoremap <leader>d  :<C-u>Wd<CR>
nnoremap <leader>D  :<C-u>bd!<CR>
nnoremap <leader>/  :Commentary<CR>
xnoremap <leader>/  :Commentary<CR>

" Coc mappings c
nnoremap <leader>c.  :<C-u>CocConfig<CR>
nnoremap <leader>cz  :<C-u>CocDisable<CR>
nnoremap <leader>cZ  :<C-u>CocEnable<CR>
nnoremap <leader>cd  :<C-u>CocList diagnostics<CR>
nnoremap <leader>ce  :<C-u>CocList extensions<CR>
nnoremap <leader>cc  :<C-u>CocList commands<CR>
nnoremap <leader>co  :<C-u>CocList outline<CR>
nnoremap <leader>cs  :<C-u>CocList -I symbols<CR>
nnoremap <leader>cj  :<C-u>CocNext<CR>
nnoremap <leader>ck  :<C-u>CocPrev<CR>
nnoremap <leader>cp  :<C-u>CocListResume<CR>
nmap <leader>cr  <Plug>(coc-rename)
nmap <leader>cf  <Plug>(coc-format)
xmap <leader>cF  <Plug>(coc-format-selected)
nmap <leader>cF  <Plug>(coc-format-selected)
nmap <leader>ca  <Plug>(coc-codeaction)
xmap <leader>cA  <Plug>(coc-codeaction-selected)
nmap <leader>cA  <Plug>(coc-codeaction-selected)
nmap <leader>cq  <Plug>(coc-fix-current)
nmap <leader>c[  <Plug>(coc-diagnostic-prev)
nmap <leader>c]  <Plug>(coc-diagnostic-next)
nmap <leader>cgd <Plug>(coc-definition)
nmap <leader>cgy <Plug>(coc-type-definition)
nmap <leader>cgi <Plug>(coc-implementation)
nmap <leader>cgr <Plug>(coc-references)
nmap <leader>cl  <Plug>(coc-range-select)
xmap <leader>cl  <Plug>(coc-range-select)
nnoremap <leader>ct :call <SID>show_documentation()<CR>

" Fzf search f
nnoremap <leader>ff  :<C-u>Files<CR>
nnoremap <leader>fg  :<C-u>GFiles<CR>
nnoremap <leader>fb  :<C-u>Buffers<CR>
nnoremap <leader>fr  :<C-u>Rg<CR>
nnoremap <leader>fl  :<C-u>Lines<CR>
nnoremap <leader>fh  :<C-u>History<CR>
nnoremap <leader>fs  :<C-u>History/<CR>
nnoremap <leader>fc  :<C-u>Commands<CR>
nnoremap <leader>fm  :<C-u>Maps<CR>

" Git mappings g

" Wiki mappings w
nmap <localleader>wj  <Plug>VimwikiNextLink
nmap <localleader>wk  <Plug>VimwikiPrevLink
nmap <localleader>wH  <Plug>Vimwiki2HTMLBrowse
nmap <localleader>wg  <Plug>VimwikiGoto
nmap <localleader>wx  <Plug>VimwikiDeleteFile
nmap <localleader>wdi <Plug>VimwikiDiaryGenerateLinks
nmap <localleader>wdm <Plug>VimwikiMakeTomorrowDiaryNote
nmap <localleader>wdt <Plug>VimwikiTabMakeDiaryNote
nmap <localleader>wdw <Plug>VimwikiMakeDiaryNote
nmap <localleader>wdy <Plug>VimwikiMakeYesterdayDiaryNote

""""""""""""""""""""""""""""""""""""""""""""""""""
"                    Pluings                     "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Auto install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

  " Coc language client
  Plug 'neoclide/coc.nvim', {'branch': 'release'}

  " Fzf fuzzy search
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'

  " Polyglot syntax highlight
  Plug 'sheerun/vim-polyglot'

  " Gruvbox color theme
  Plug 'morhetz/gruvbox'

  " Airline bar
  " replace with lightline, lightline-bufferline and lightline-gruvbox
  Plug 'vim-airline/vim-airline'

  " Which-key show keybinds
  Plug 'liuchengxu/vim-which-key'

  " Fugitive git integration
  Plug 'tpope/vim-fugitive'

  " Commentary comments
  Plug 'tpope/vim-commentary'

  " Vimtex latex
  Plug 'lervag/vimtex'

  " Vimwiki notes
  Plug 'vimwiki/vimwiki'

  " nvim-colorizer

  " Lens.vim
  "
  " Floatterm + lazygit

  " Vimspector

  " Pear-tree/auto-pairs

  " Vim wiki

  " Startify eller gem sidste session ligesom sublime text

  " Coc-explorer eller nerd tree

  " Coqtail

  " Vim-resize

  " Coc-fzf

  " Vista

  " Ale

  " Emment-vim

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""
"                  Misc config                   "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Gruvbox color theme
colorscheme gruvbox

" Gruvbox enable italic text
let g:gruvbox_italic = 1

" Airline enable tabline
let g:airline#extensions#tabline#enabled = 1

" Vimtex set tex flavour and pdf reader
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'

" Vim wiki
let g:vimwiki_table_mappings=0

""""""""""""""""""""""""""""""""""""""""""""""""""
"                Which-key config                "
""""""""""""""""""""""""""""""""""""""""""""""""""

let g:which_key_map = {}

let g:which_key_map['.'] = 'config'
let g:which_key_map['/'] = 'comment (gc)'
let g:which_key_map.i = 'invert theme'
let g:which_key_map.s = 'save'
let g:which_key_map.S = 'sudo save'
let g:which_key_map.q = 'save and quit'
let g:which_key_map.Q = 'force quit'
let g:which_key_map.d = 'save and close'
let g:which_key_map.D = 'force close'
let g:which_key_map.c = {
    \ 'name' : '+coc',
    \ '.' : 'config',
    \ 'z' : 'disable coc',
    \ 'Z' : 'enable coc',
    \ 'd' : 'diagnostics',
    \ 'e' : 'extensions',
    \ 'c' : 'commands',
    \ 'o' : 'outline',
    \ 's' : 'symbols',
    \ 'j' : 'next action',
    \ 'k' : 'prev action',
    \ 'p' : 'resume list',
    \ 'r' : 'rename',
    \ 'f' : 'format buffer',
    \ 'F' : 'format selected',
    \ 'a' : 'action line',
    \ 'A' : 'action selected',
    \ 'q' : 'quick fix',
    \ 'g' : {
        \ 'name' : '+goto',
        \ 'd' : 'definition (gd)',
        \ 'y' : 'type definition (gy)',
        \ 'i' : 'implementation (gi)',
        \ 'r' : 'references (gr)',
        \ },
    \ '[' : 'diagnostic prev ([g)',
    \ ']' : 'diagnostic next (]g)',
    \ 't' : 'documentation (K)',
    \ 'l' : 'range select (<C-s>)',
    \ }
let g:which_key_map.f = {
    \ 'name' : '+fzf',
    \ 'f' : 'files',
    \ 'g' : 'git files',
    \ 'b' : 'buffers',
    \ 'l' : 'lines',
    \ 'r' : 'ripgrep',
    \ 'h' : 'file history',
    \ 's' : 'search history',
    \ 'c' : 'commands',
    \ 'm' : 'mappings',
    \ }
let g:which_key_map.l = {
    \ 'name' : '+tex',
    \ 'i' : 'info',
    \ 'I' : 'info full',
    \ 't' : 'toc open',
    \ 'T' : 'toc toggle',
    \ 'y' : 'labels open',
    \ 'Y' : 'labels toggle',
    \ 'v' : 'view',
    \ 'r' : 'reverse search',
    \ 'l' : 'compile toggle',
    \ 'L' : 'compile selected',
    \ 'k' : 'stop',
    \ 'K' : 'stop all',
    \ 'e' : 'errors',
    \ 'o' : 'compile output',
    \ 'g' : 'status',
    \ 'G' : 'status all',
    \ 'c' : 'clean',
    \ 'C' : 'clean full',
    \ 'm' : 'imaps list',
    \ 'x' : 'reload',
    \ 'X' : 'reload state',
    \ 's' : 'toggle main',
    \ 'q' : 'log',
    \ }
let g:which_key_map.w = {
    \ 'name' : '+wiki',
    \ 'x' : 'delete file',
    \ 'h' : 'to html',
    \ 'H' : 'to html browse',
    \ 'i' : 'dairy index',
    \ 'j' : 'next link',
    \ 'k' : 'prev link',
    \ 'g' : 'goto',
    \ 'r' : 'rename file',
    \ 's' : 'select wiki',
    \ 't' : 'tab index',
    \ 'w' : 'index',
    \ 'd' : {
        \ 'name' : '+diary',
        \ 'i' : 'diary generate links',
        \ 'm' : 'make tomorrow diary note',
        \ 't' : 'tab make diary note',
        \ 'w' : 'make diary note',
        \ 'y' : 'make yesterday diary note',
        \ },
    \ }

call which_key#register('<Space>', "g:which_key_map")

nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :<c-u>WhichKeyVisual '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey '<Space>'<CR>
vnoremap <silent> <localleader> :<c-u>WhichKeyVisual '<Space>'<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""
"                   Coc config                   "
""""""""""""""""""""""""""""""""""""""""""""""""""

" Coc extensions
let g:coc_global_extensions = [
    \ 'coc-json',
    \ 'coc-vimlsp',
    \ 'coc-vimtex',
    \ 'coc-rust-analyzer',
    \ 'coc-python',
    \ 'coc-go',
    \ 'coc-metals',
    \ ]

" TextEdit might fail if hidden is not set.
"set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
"nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
"xmap <leader>f  <Plug>(coc-format-selected)
"nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
"xmap <leader>a  <Plug>(coc-codeaction-selected)
"nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
"nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
"nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of LS, ex: coc-tsserver
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
"nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
"nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
"nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
"nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
"nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
"nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
"nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
"nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

# Config files
## Intro
Config files for Arch + Sway setup.

## Install
* git clone https://gitlab.com/jkbch/.config.git ~/.config
* ln -s ~/.config/bin ~/.local/bin
* ln -s ~/.config/applications ~/.local/share/applications
* ln -s ~/.config/mimeapps.list ~/.local/share/applications/mimeapps.list
* sudo ln -s ~/.config/zi /usr/share/X11/xkb/symbols/zi

# Neovim setup
yay -S neovim node python-pynvim python go scala sbt stack-bin haskell-ide-engine

## Programs
### Desktop enviroment
sway waybar alacritty networkmanager pulseaudio pavucontrol tlp j4-dmenu-desktop bemenu autotiling imagemagick grim swaylock swayidle

### Applications
neovim nnn yay firefox zathura imv neomutt emacs(doom)
